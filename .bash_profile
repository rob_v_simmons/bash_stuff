if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

set -o vi
shopt -s extglob
PS1='\[\033[7m\]: \u@macbook \w;\[\033[m\] '

export PATH="$PATH:/Users/rsimmons/bin"

#############
# functions #
#############

#cd /Users/rsimmons/Sites

echo "almost to infinity! definitely not beyond."
