set nu
set ruler
nnoremap / /\v
cnoremap %s/ %s/\v
cnoremap s/ s/\v
filetype plugin indent on
syntax on

set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
