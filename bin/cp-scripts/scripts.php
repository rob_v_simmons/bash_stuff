<?php

/**
 * scripts.php
 *
 * Author: Rob Simmons
 *
 * List all Customer Paradigm commands, scripts, functions, aliases, etc, and helpful 
 * information on each.
 *
 */

$addMode = false;

if ($argc > 2) {
    die('usage: ' . $argv[0] . " [command]\n");
} elseif ($argc === 2) {
    if (strtolower($argv[1]) === 'add') {
        $requestedCommand = false;
        $addMode = true;
    } else {
        $requestedCommand = $argv[1];
    }
} else {
    $requestedCommand = false;
}

// todo have ADD as a param to this script, or create a scripts-add.php ??

require_once('db-model.php');
$model = new Scripts_Db_Model();


if (!$addMode) {
    if ($requestedCommand) {
        $commandsData = $model->getCommand($requestedCommand);
    } else {
        $commandsData = $model->getAllCommands();
    }

    if (!$requestedCommand) {
        echo "This script lists our available custom scripts, functions, aliases, etc.\n";
        echo "If you pass a command as a param, in-depth information on just that command is listed.\n\n";
        echo "Usage:\n";
        echo "\t scripts [command]\n";
        echo "\t scripts add\n\n";
        echo "Available Commands:\n";
        echo "-------------------\n";
    }


    foreach ($commandsData as $commandData) {
        echo $commandData['command_name'] . "\n";
        echo "\t" . (($requestedCommand && isset($commandData['description'])) ? $commandData['description'] : $commandData['short_description']) . "\n";
        if ($commandData['usage_generic']) {
            if ($requestedCommand) {
                echo "\n";
            }
            echo "\tUsage: " . $commandData['usage_generic'] . "\n";
        }

        if ($requestedCommand) {
            foreach ($commandData['usages'] as $usageData) {
                echo "\t\te.g. " . $usageData['usage_text'] . "\n";
                echo "\t\t\t" . $usageData['usage_description'] . "\n";
            }
        }
    }
}

else { // add mode

    // todo test

    // command
    $name = readline('Enter command name: ');
    $shortDescription = readline('Enter short description: ');
    $description = readline('Enter optional long description: ');
    $usageGeneric = readline('Enter optional generic usage: ');


    // usages
    $addUsagePrompt = 'Type "y" to add a usage example: ';
    $shouldAddUsage = strtolower(readline($addUsagePrompt));
    $usages = array();

    while ($shouldAddUsage === 'y') {
        $usageText = readline("Enter usage text: ");
        $usageDescription = readline("Enter usage description: ");
        $usages[$usageText] = $usageDescription;

        $shouldAddUsage = strtolower(readline($addUsagePrompt));
    }

    $model->addCommand($name, $shortDescription, $description, $usageGeneric, $usages);

    echo "\nAdded $name command\n";
}
