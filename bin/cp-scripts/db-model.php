<?php

class Scripts_Db_Model
{
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = '';
    const DB_NAME = 'cp_scripts';

    protected $_mysql;

    /**
     * Create connection to DB, and create our tables if they don't exist
     */
    public function __construct()
    {
        $this->_mysql = new mysqli(self::DB_HOST, self::DB_USER, self::DB_PASS, self::DB_NAME);

        if ($this->_mysql->connect_error) {
            die("Problem connecting to DB (" . $this->_mysql->connect_errno . ') ' . $this->_mysql->connect_error);
        }

        $createCommandsTableSql = "CREATE TABLE IF NOT EXISTS commands(
            command_id INT NOT NULL AUTO_INCREMENT,
            command_name VARCHAR(40) NOT NULL,
            short_description VARCHAR(256) NOT NULL,
            description VARCHAR(512),
            usage_generic VARCHAR(100),
            PRIMARY KEY (command_id)
        ) ENGINE=INNODB;";


        $createUsagesTableSql = "CREATE TABLE IF NOT EXISTS usages(
            usage_id INT NOT NULL AUTO_INCREMENT,
            command_id INT NOT NULL,
            usage_text VARCHAR(256) NOT NULL,
            usage_description VARCHAR(512),
            PRIMARY KEY (usage_id),
            FOREIGN KEY (command_id)
                REFERENCES commands(command_id)
                ON UPDATE CASCADE ON DELETE CASCADE
        ) ENGINE=INNODB;";

        $this->_mysql->query($createCommandsTableSql);
        $this->_mysql->query($createUsagesTableSql);
    }

    /**
     * Close DB connection
     */
    public function __destruct()
    {
        $this->_mysql->close();
    }



    /**
     * Get all data (except usage examples) for all commands
     *
     * @return array
     */
    public function getAllCommands()
    {
        $sql = "SELECT * FROM commands";
        $result = $this->_mysql->query($sql);
        $entries = array();
        while ($row = $result->fetch_assoc()) {
            $row['usages'] = array();
            $entries[] = $row;
        }
        $result->close();
        return $entries;
    }

    /**
     * Get all data (including usage examples) for the specified command
     *
     * @param string $command
     * @return array
     */
    public function getCommand($command)
    {
        $commandsSql = "SELECT * FROM commands WHERE command_name = '$command'";
        $commandsResult = $this->_mysql->query($commandsSql);
        if (!$commandsResult) {
            return false;
        }
        $commandsEntry = $commandsResult->fetch_assoc();
        $commandsResult->close();
        if (!$commandsEntry) {
            return false;
        }

        $commandId = $commandsEntry['command_id'];
        $usagesSql = "SELECT * FROM usages WHERE command_id = $commandId";
        $usagesResult = $this->_mysql->query($usagesSql);

        $usages = array();
        if ($usagesResult) {
            while ($usagesRow = $usagesResult->fetch_assoc()) {
                $usages[] = $usagesRow;
            }
            $usagesResult->close();
        }

        $commandsEntry['usages'] = $usages;
        $entries = array($commandsEntry);
        return $entries;
    }



    /**
     * Add a command (and it's usages)
     *
     * @param string $name
     * @param string $shortDescription
     * @param string $description
     * @param string $usageGeneric
     * @param array $usages -- usage_text => usage_description
     * @return int
     */
    public function addCommand($name, $shortDescription, $description = '', $usageGeneric = '', $usages = array())
    {
        $sql = "INSERT INTO commands (command_name, short_description, description, usage_generic)
            VALUES ('$name', '$shortDescription', '$description', '$usageGeneric');";
        $this->_mysql->query($sql);
        $commandId = $this->_mysql->insert_id;

        foreach ($usages as $usageText => $usageDescription) {
            $this->_addCommandUsageExample($commandId, $usageText, $usageDescription);
        }

        return $commandId;
    }

    /**
     * Add a usage example to a command
     *
     * @param int $commandId
     * @param string $usageText
     * @param string $usageDescription
     */
    protected function _addCommandUsageExample($commandId, $usageText, $usageDescription)
    {
        $sql = "INSERT INTO usages (command_id, usage_text, usage_description)
            VALUES ($commandId, '$usageText', '$usageDescription')";
        $this->_mysql->query($sql);
    }
}
