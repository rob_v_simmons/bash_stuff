alias l=ls
alias ls='ls -Gp'
alias ll='ls -l'
alias la='ls -A'

alias c=cd

# edit .bash_profile, .vimrc, .bash_aliases
alias eb='vi ~/.bash_profile'
alias ev='vi ~/.vimrc'
alias eba='vi ~/.bash_aliases'

# source .bash_profile, .vimrc, .bash_aliases
alias .b='. ~/.bash_profile; echo Sourced .bash_profile'
alias .v='. ~/.vimrc; echo Sourced .vimrc'
alias .ba='. ~/.bash_aliases; echo Sourced .bash_aliases'

alias ssh-config='vi /Users/rsimmons/.ssh/config'

alias sites='cd ~/Sites; ls'
alias bin='cd ~/bin; ls'

alias hosts='sudo vi /etc/hosts'
alias ini='sudo vi /etc/php.ini'
alias vhosts='sudo vi /etc/apache2/extra/httpd-vhosts.conf'

alias apache-conf='sudo vi /etc/apache2/httpd.conf'
alias apache-restart='sudo apachectl restart'
alias apache-start='sudo apachectl start'
alias apache-stop='sudo apachectl stop'

alias mysql='mysql -uroot'
alias mysqldump='mysqldump -uroot'
alias mysqladmin='mysqladmin -uroot'

alias mt='~/bin/n98-magerun.phar'

alias untarzip='tar -zxvf'
alias tarzip='tar -zcvf'

# easydev2 xdebug ssh tunnel
#alias ed2x='ssh -R 9002:localhost:9000 go.easydev.us'

alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'

alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

alias ss='cd ~/rvs-games/space-samurai/; java SpaceSamurai'

alias cron_list_all='for user in $(cut -f1 -d: /etc/passwd); do echo $user; sudo crontab -u $user -l; done'
